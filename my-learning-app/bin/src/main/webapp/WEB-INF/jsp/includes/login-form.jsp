<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c"  %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix = "fn"  uri = "http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>  


<div class="container">

	    <div class="row">
    
	    <div class="col-lg-8 col-lg-offset-2" >
	    
	       <div class="text-center">
	          <h3>Formulario de acceso</h3>
	          <form:form id="login-form"  action="/login" method="post" role="form" 
	             modelAttribute="userLogin"
	          >
	           <div class="form-group">
	              <form:input type="email" name="email" id="email" tabIndex="1" class="form-control"   path="email" placeholder="Email" required="required"
	                     autofocus="autofocus" />
	           </div>
	           <div class="form-group">
	              <form:input type="password" name="password" id="password" tabIndex="2" class="form-control"   path="password" placeholder="Password"  required="required" />
	           </div>
	            <div class="form-group">
	              <button type="submit" name="login-submit" id="login-submit" tabIndex="3" class="btn btn-lg btn-primary btn-block" >Login</button>               
	           </div>          
	          </form:form>
	       </div>    
	    </div>
    </div>

</div>