<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<nav
	class="navbar navbar-toggleable-md navbar-inverse bg-primary bg-faded">
	<button class="navbar-toggler navbar-toggler-right" type="button"
		data-toggle="collapse" data-target="#navbarNav"
		aria-controls="navbarNav" aria-expanded="false"
		aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<a class="navbar-brand" href="/"> <img
		src="https://freeiconshop.com/wp-content/uploads/edd/book-flat.png"
		width="30" height="30" class="d-inline-block align-top" alt="">
		e-Learning
	</a>
	<div class="collapse navbar-collapse" id="navbarNav">
		<ul class="navbar-nav">
			<li ${not empty param.home ?  'class="active"' : '' }
				class="nav-item"><a class="nav-link" href="/">Home <span
					class="sr-only">(current)</span>
			</a></li>
			<li class="nav-item"><a class="nav-link" href="/professors">Professors</a>
			</li>


			<c:choose>
				<c:when test="${not empty sessionScope.userLoggedIn }">

					<li class="nav-item"><a class="nav-link" href="/my-questions">New
							Question</a></li>

					<li class="nav-item"><a class="nav-link" href="/logout">Logout</a>
					</li>
				</c:when>
				<c:otherwise>
					<li class="nav-item"><a class="nav-link" href="/signin">Login</a>
					</li>
				</c:otherwise>
			</c:choose>
		</ul>
	</div>
</nav>