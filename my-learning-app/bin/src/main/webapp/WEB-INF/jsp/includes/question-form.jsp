<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<div class="container">

	<div class="text-center">
		<h3>Add your Question</h3>
		<form:form modelAttribute="questionbean" id="question-form"
			action="/submit/newquestion" method="POST" role="form"
			autocomplete="off">
			<div class="form-group">
				<form:input type="text" name="title" id="title" tabIndex="1"
					class="form-control" path="title" placeholder="Title" />
			</div>
			
			<div class="form-group">
				<form:input type="text" name="content" id="content" tabIndex="1"
					class="form-control" path="content" placeholder="Question" />
			</div>

			<div class="form-group">
				<input type="submit" class="form-control btn btn-info"
					value="Register Now" />
			</div>

		</form:form>
	</div>


</div>
