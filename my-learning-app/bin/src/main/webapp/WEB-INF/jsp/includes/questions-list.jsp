<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>


<c:forEach items="${questionlist}" var="questionlist">
	<div class="jumbotron">
		<h1>
			<a href="/question/${questionlist.id}">${questionlist.title}</a>
		</h1>
		<p class="lead">${questionlist.content}</p>
		<hr class="my-4">
		<p>Date: ${questionlist.date}</p>
		<p class="lead">
			<a class="btn btn-primary btn-lg" href="#" role="button"> Want a
				chance?</a>
		</p>
	</div>
</c:forEach>