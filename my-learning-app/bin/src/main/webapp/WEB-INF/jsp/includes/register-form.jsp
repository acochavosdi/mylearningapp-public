<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>


<div class="container">

	<div class="text-center">
		<h3>Registro</h3>
		<form:form id="register-form" action="/register" method="post"
			role="form" autocomplete="off" modelAttribute="userRegister">
			<div class="form-group">
				<form:input type="text" name="name" id="name" tabIndex="1"
					class="form-control" path="name" placeholder="Name" />
			</div>

			<div class="form-group">
				<form:input type="email" name="email" id="email" tabIndex="2"
					class="form-control" path="email" placeholder="Email" />
			</div>

			<div class="form-group">
				<form:input type="password" name="password" id="password"
					tabIndex="3" class="form-control" path="Password"
					placeholder="Password" />
			</div>

			<div class="form-group">
				<input type="password" name="confirm-password" id="confirm-password"
					tabIndex="4" class="form-control" placeholder="Repeat Password"
					data-rule-equalTo="#password" />
			</div>

			<div class="form-group">
				<input type="submit" name="register-submit" id="register-submit"
					tabIndex="5" class="form-control btn btn-info"
					value="Registrar ahora" />
			</div>

		</form:form>
	</div>


</div>

