<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>


<div class="jumbotron jumbotron-fluid">
	<div class="container">
		<h1 class="display-3">Welcome to my Learning App</h1>
		
		<p class="lead">If you are new to our app, we invite you to
			register</p>
		<p>
			<a href="/signup" role="button" class="btn btn-lg btn-success ">Register</a>
		</p>

	</div>
</div>