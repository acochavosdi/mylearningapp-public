<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>





<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>my Learning App</title>

<!--  STYLESHEETS -->
<jsp:include page="includes/imports-css.jsp" />
</head>
<body>

	<!--  MAIN CONTAINER -->
	<div class="container">
<jsp:param name="professors"  value="professors" />
		<!--  MAIN MENU -->
		<jsp:include page="includes/menu.jsp" />


		<!--  Professors List Block -->

		<div class="container-fluid m-5">
			<div class="row justify-content-center">
				<c:forEach items="${professors}" var="professors">
					<div class="card p-3 m-3" style="width: 20rem;">
						<img class="card-img-top mx-auto rounded-circle"
							src="http://i.pravatar.cc/150?u=${professors.email}"
							alt="Card image cap" >
						<div class="card-block">
							<h4 class="card-title">${professors.name}</h4>
						</div>
						<ul class="list-group list-group-flush">
							<li class="list-group-item">${professors.email}</li>
							<li class="list-group-item"><fmt:formatDate
									pattern="dd/MM/yyyy" value="${professors.entryDate}" /></li>
						</ul>
					</div>
				</c:forEach>
			</div>
		</div>
	</div>



	<!--  Javascript  -->
	<jsp:include page="includes/imports-js.jsp" />

</body>
</html>