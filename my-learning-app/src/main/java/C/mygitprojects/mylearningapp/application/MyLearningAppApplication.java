package C.mygitprojects.mylearningapp.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyLearningAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyLearningAppApplication.class, args);
	}
}
