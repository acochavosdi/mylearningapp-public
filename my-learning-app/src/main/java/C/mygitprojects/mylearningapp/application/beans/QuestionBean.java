package C.mygitprojects.mylearningapp.application.beans;

public class QuestionBean {

	private String title;
	private String content;

	public QuestionBean() {

	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}
