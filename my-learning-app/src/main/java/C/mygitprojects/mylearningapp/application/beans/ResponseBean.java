package C.mygitprojects.mylearningapp.application.beans;

public class ResponseBean {
	
	private String content;
	private long question_id;

	public ResponseBean() {
		super();
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public long getQuestion_id() {
		return question_id;
	}

	public void setQuestion_id(long question_id) {
		this.question_id = question_id;
	}

	

	

}
