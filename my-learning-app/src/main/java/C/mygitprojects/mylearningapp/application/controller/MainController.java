package C.mygitprojects.mylearningapp.application.controller;


import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import C.mygitprojects.mylearningapp.application.dao.QuestionDao;
import C.mygitprojects.mylearningapp.application.model.Professor;

@Controller
public class MainController {
	
	@Autowired
	private HttpSession httpSession;

	
	@Autowired
	private QuestionDao questionDao;
	
	@GetMapping(value="/")
	public String welcome(Model modelo) {
		
		modelo.addAttribute("user_active",(Professor) httpSession.getAttribute("userLoggedIn"));
		modelo.addAttribute("questionlist", questionDao.getAll());
		return "index";
		
	}
}
