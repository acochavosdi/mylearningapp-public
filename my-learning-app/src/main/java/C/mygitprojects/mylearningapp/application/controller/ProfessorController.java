package C.mygitprojects.mylearningapp.application.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import C.mygitprojects.mylearningapp.application.dao.ProfessorDao;





@Controller
public class ProfessorController {
	@Autowired
	private ProfessorDao professorDao;

	@GetMapping(value = "/professors")
	public String listprofessors(Model modelo) {
		modelo.addAttribute("professors", professorDao.getAll());
		return "professorslist";
	}
}
