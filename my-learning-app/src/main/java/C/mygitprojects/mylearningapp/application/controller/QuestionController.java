package C.mygitprojects.mylearningapp.application.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import C.mygitprojects.mylearningapp.application.beans.QuestionBean;
import C.mygitprojects.mylearningapp.application.beans.ResponseBean;
import C.mygitprojects.mylearningapp.application.dao.QuestionDao;
import C.mygitprojects.mylearningapp.application.dao.ResponseDao;
import C.mygitprojects.mylearningapp.application.model.Professor;
import C.mygitprojects.mylearningapp.application.model.Question;
import C.mygitprojects.mylearningapp.application.model.Response;

@Controller
public class QuestionController {

	@Autowired
	private QuestionDao questionDao;

	@Autowired
	private ResponseDao responseDao;

	@Autowired
	private HttpSession httpSession;

	@GetMapping(value = "/my-questions")
	public String showForm(Model modelo) {
		modelo.addAttribute("questionbean", new QuestionBean());
		return "my-questions";
	}

	@PostMapping(value = "/submit/newquestion")
	public String submit(@ModelAttribute("questionbean") QuestionBean questionBean, Model model) {

		Question question = new Question();
		question.setContent(questionBean.getContent());

		Professor professor = (Professor) httpSession.getAttribute("userLoggedIn");
		question.setTitle(questionBean.getTitle());
		question.setProfessor(professor);
		questionDao.create(question);
		// autor.getPosts().add(post);

		return "redirect:/";
	}

	@GetMapping(value = "/question/{id}")
	public String detail(@PathVariable("id") long id, Model modelo) {

		modelo.addAttribute("questionbean", new QuestionBean());
		modelo.addAttribute("responsebean", new ResponseBean());
		// TESTS IF QUESTION EXISTS
		Question result = null;
		if ((result = questionDao.getById(id)) != null) {
			modelo.addAttribute("questionbean", result);
			modelo.addAttribute("responseForm", new ResponseBean());
			return "question";
		} else
			return "redirect:/";
	}

	@PostMapping(value = "/submit/newResponse")
	public String submitResponse(@ModelAttribute("responsebean") ResponseBean responseBean, Model model) {
	
		Professor professor = (Professor) httpSession.getAttribute("userLoggedIn");

		Response response = new Response();
		response.setProfessor(professor);


		Question question = questionDao.getById(responseBean.getQuestion_id());
		response.setQuestion(question);

		response.setContent(responseBean.getContent());
		responseDao.create(response);
		

		
		question.getResponses().add(response);
		professor.getResponses().add(response);

		//return "redirect:/post/" + responseBean.getQuestion_id();
		return "redirect:/question/" + responseBean.getQuestion_id();
	}

}
