package C.mygitprojects.mylearningapp.application.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import C.mygitprojects.mylearningapp.application.beans.RegisterBean;
import C.mygitprojects.mylearningapp.application.dao.ProfessorDao;
import C.mygitprojects.mylearningapp.application.model.Professor;

@Controller
public class RegisterController {
	@Autowired
	private ProfessorDao professorDao;

	@GetMapping(value = "/signup")
	public String showForm(Model model) {
		model.addAttribute("userRegister", new RegisterBean());
		return "register";
	}

	@PostMapping(value = "/register")
	public String submit(@ModelAttribute("userRegister") RegisterBean r, Model model) {
		professorDao.create(new Professor(r.getName(), r.getEmail(), r.getPassword()));

		return "redirect:/";
	}
}
