package C.mygitprojects.mylearningapp.application.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import C.mygitprojects.mylearningapp.application.model.Professor;

@Repository
@Transactional
public class ProfessorDao {

	@PersistenceContext
	private EntityManager entityManager;

	/*
	 * Store the Professor on the BBDD
	 */
	public void create(Professor profesor) {
		entityManager.persist(profesor);
		return;
	}


	@SuppressWarnings("unchecked")
	public List<Professor> getAll() {
		return entityManager.createQuery("select u from Professor u").getResultList();
	}

	/*
	 * Validate Professor using email + password
	 */
	public Professor getByEmailAndPassword(String email, String password) {
		Professor resultado = null;
		try {
			resultado = (Professor) entityManager
					.createNativeQuery("select * FROM Professor where  email= :email and password=md5(:password)",
							Professor.class)
					.setParameter("email", email).setParameter("password", password).getSingleResult();
		} catch (NoResultException e) {
			resultado = null;
		}
		return resultado;

	}

}
