package C.mygitprojects.mylearningapp.application.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import C.mygitprojects.mylearningapp.application.model.Question;

@Repository
@Transactional
public class QuestionDao {

	@PersistenceContext
	private EntityManager entityManager;

	/*
	 * Store question on bbdd
	 */
	public void create(Question question) {
		entityManager.persist(question);
		return;
	}

	@SuppressWarnings("unchecked")
	public List<Question> getAll() {
		return entityManager.createQuery("select p from Question p").getResultList();
	}

	/**
	 * Give back the question from id
	 */
	public Question getById(long id) {
		return entityManager.find(Question.class, id);
	}

}
