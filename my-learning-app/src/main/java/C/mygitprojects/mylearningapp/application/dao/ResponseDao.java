package C.mygitprojects.mylearningapp.application.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import C.mygitprojects.mylearningapp.application.model.Response;

@Repository
@Transactional
public class ResponseDao {

	@PersistenceContext
	private EntityManager entityManager;

	/*
	 * Store Response on BBDD
	 */
	public void create(Response response) {
		entityManager.persist(response);
		return;
	}

	@SuppressWarnings("unchecked")
	public List<Response> getAll() {
		return entityManager.createQuery("select c from Response c").getResultList();
	}

	/**
	 * Get Back a response from his id
	 */
	public Response getById(long id) {
		return entityManager.find(Response.class, id);
	}

}
