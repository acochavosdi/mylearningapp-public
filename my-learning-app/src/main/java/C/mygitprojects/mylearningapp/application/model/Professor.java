package C.mygitprojects.mylearningapp.application.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.annotations.ColumnTransformer;
import org.hibernate.annotations.CreationTimestamp;

@Entity
public class Professor {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(name = "name")
	private String name;

	@Column
	private String email;

	@Column
	@CreationTimestamp
	private Date entryDate;

	@Column
	@ColumnTransformer(write = " MD5(?) ")
	private String password;

	@OneToMany(mappedBy = "professor", fetch = FetchType.EAGER)
	private Set<Response> response = new HashSet<>();

	@OneToMany(mappedBy = "professor", fetch = FetchType.EAGER)
	private Set<Question> question = new HashSet<>();

	public Professor() {
	}

	public Professor(String name, String email, String password) {
		super();
		this.name = name;
		this.email = email;
		this.password = password;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getEntryDate() {
		return entryDate;
	}

	public void setEntryDate(Date entryDate) {
		this.entryDate = entryDate;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Set<Response> getResponses() {
		return response;
	}

	public void setResponses(Set<Response> response) {
		this.response = response;
	}

	public Set<Question> getQuestions() {
		return question;
	}

	public void setQuestions(Set<Question> question) {
		this.question = question;
	}

}
