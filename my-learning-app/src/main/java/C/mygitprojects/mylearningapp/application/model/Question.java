package C.mygitprojects.mylearningapp.application.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;

@Entity
@Table(name = "questions")
public class Question {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column
	private String title;

	@Column
	private String content;

	@Column
	@CreationTimestamp
	private Date date;

	@ManyToOne
	private Professor professor;

	@OneToMany(mappedBy = "question")
	private List<Response> response = new ArrayList<>();

	public Question() {
		super();
	}

	public Question(String title, String content, Professor professor) {
		super();
		this.title = title;
		this.content = content;
		this.professor = professor;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Professor getProfessor() {
		return professor;
	}

	public void setProfessor(Professor professor) {
		this.professor = professor;
	}

	public List<Response> getResponses() {
		return response;
	}

	public void setQuestion(List<Response> response) {
		this.response = response;
	}

}
