package C.mygitprojects.mylearningapp.application.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

@Entity
public class Response {
	@Id
	@GeneratedValue
	private long id;

	@Column
	@Lob
	private String content;

	@Column
	private boolean valid_response;

	@ManyToOne
	@JoinColumn(name = "professor_id", updatable = false)
	private Professor professor;

	@ManyToOne
	@JoinColumn(name = "question_id", updatable = false)
	private Question question;

	public Response() {
		super();
	}

	public Response(String content, boolean valid_response, Professor professor, Question question) {
		super();
		this.content = content;
		this.valid_response = valid_response;
		this.professor = professor;
		this.question = question;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public boolean isValid_response() {
		return valid_response;
	}

	public void setValid_response(boolean valid_response) {
		this.valid_response = valid_response;
	}

	public Professor getProfessor() {
		return professor;
	}

	public void setProfessor(Professor professor) {
		this.professor = professor;
	}

	public Question getQuestion() {
		return question;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}

}
