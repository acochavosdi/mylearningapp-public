package C.mygitprojects.mylearningapp.application.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.ColumnTransformer;
import org.hibernate.annotations.CreationTimestamp;

@Entity
public class Student {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(name = "name")
	private String name;

	@Column
	private String email;

	@Column
	@CreationTimestamp
	private Date entryDate;

	@Column
	@ColumnTransformer(write = " MD5(?) ")
	private String password;

}
