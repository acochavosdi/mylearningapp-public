<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<div class="container mt-4">

	<div class="text-center">
		<h3>Add your Response</h3>
		<form:form modelAttribute="responsebean" id="question-form"
			action="/submit/newResponse" method="POST" role="form"
			autocomplete="off">
			
			<form:input type="hidden" id="question_id" name="question_id" path="question_id" value="${questionbean.id}" />
			<div class="form-group">
				<form:input type="text" name="content" id="content" tabIndex="1"
					class="form-control" path="content" placeholder="Question" />
			</div>

			<div class="form-group">
				<input type="submit" class="form-control btn btn-primary"
					value="Add response now" />
			</div>
		</form:form>
	</div>


</div>
