<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>



<div class="container-fluid" style="margin-top: 5rem;">
	<div class="row justify-content-around flex-wrap">
		<c:forEach items="${questionlist}" var="questionlist">
			<div class="col-xl-4 col-lg-4 col-sm-6 col-xs-12">
				<div class="card mx-2 my-4" style="background-color: #0275d8; color: white">
					<h3 class="card-header">
						<a href="/question/${questionlist.id}">${questionlist.title}</a>
					</h3>
					<div class="card-block">
						<h6 class="card-title">${questionlist.content}</h6>
						<p class="card-text">Autor: ${questionlist.professor.name}</p>
						<p class="card-text">
							Fecha:
							<fmt:formatDate pattern="dd/MM/yyyy"
								value="${questionlist.date}" />
						</p>
					</div>
				</div>
			</div>
		</c:forEach>
	</div>
</div>

