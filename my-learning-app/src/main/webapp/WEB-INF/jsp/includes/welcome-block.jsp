<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>


<div class="jumbotron jumbotron-fluid">

	<c:choose>
		<c:when test="${not empty sessionScope.userLoggedIn }">
			<div class="container" style="padding: 1em;">
				<h1 class="display-3">Welcome ${userLoggedIn.name}</h1>

				<p class="lead">If you want to review all the questions of our
					amazing professors, just click!!</p>
			
					<a id="button_scrolling" role="button"
						class="btn btn-lg btn-primary " style="color: white;" >Questions</a>
			
			</div>
		</c:when>
		<c:otherwise>
			<div class="container" style="padding: 1em;">
				<h1 class="display-3">Welcome to my Learning App</h1>

				<p class="lead">If you are new to our application, we invite you
					to registration, or you can simply review our questions and answer
					them so you can be tested by our professionals</p>
				<div class="row justify-content-around">
					<a href="/signup" role="button" class="btn btn-lg btn-success ">Register</a>
					<a id="button_scrolling" role="button"
						class="btn btn-lg btn-primary " style="color: white;">Questions</a>
				</div>

			</div>
		</c:otherwise>
	</c:choose>



</div>