<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>myLearningApp</title>

<!--  STYLESHEETS -->
<jsp:include page="includes/imports-css.jsp" />
</head>
<body>

	<!--  MAIN CONTAINER -->
	<div class="container-fluid p-0">

		<!--  HEADER IMAGE -->
		<jsp:include page="includes/static-content/header-image.jsp" />
		
		<!--  MAIN MENU -->
		<jsp:include page="includes/menu.jsp">
			<jsp:param name="newquestion" value="newquestion" />
		</jsp:include>

		<!--  QUESTION FORM -->
		<c:if test="${not empty sessionScope.userLoggedIn }">
			<jsp:include page="includes/forms/question-form.jsp" />
		</c:if>

		<!--  FOOTER -->
		<jsp:include page="includes/static-content/footer.jsp" />

	</div>

	<!--  JAVASCRIPT  -->
	<jsp:include page="includes/imports-js.jsp" />
</body>
</html>